import { MapsAPILoader } from 'angular2-google-maps/core/services/maps-api-loader/maps-api-loader';
import { Component, NgZone } from '@angular/core';
import { SebmGoogleMap } from 'angular2-google-maps/core';
import { SebmGoogleMapPolygon, LatLngLiteral } from 'angular2-google-maps/core';
import { MarkerModel } from './address-search-results/marker-details-model';
import { RandomMarkerGeneratorService } from './random-marker-generator.service';

declare var google: any;
const STREETVIEW_API = "https://maps.googleapis.com/maps/api/streetview?size=550x350&fov=90&heading=235&pitch=10&key=AIzaSyAEXrOEWXaG4IAi0Dp0O6-8Muoc4ljUcLs&location=";
const ICONS_API = "http://maps.google.com/mapfiles/";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements IMapper{
  title: string = 'Lexis Locate';
  activePane: number = 0;
  isChanged: number = 0;
  showDirections: boolean = false;

  lat: number = -29.812; 
  lng: number = 30.996;
  zoom: number = 15;
  bounds: any = {lat: this.lat, lng: this.lng};

  returnedMarkers: MarkerModel[] = [];
  selectedMarker: MarkerModel = new MarkerModel();
  markers: any = []

  placeholder: string = '';

  icons: string[] = ["marker_blackA.png","marker_purple.png", "marker_green.png","marker_yellow.png"]
  colors: string[] = ["","purple", "green","yellow", "green"];
  labels: string[] = ["","Sheriff House", "Magistrate Court","High Court Chambers"]

  origin = {latitude: this.lat, longitude: this.lng};
  destination = { };

  polygonColor: string = '';

  unSortedArray:Array<LatLngLiteral> = []; 
  paths: Array<LatLngLiteral> = []  
  strokeColor: string  =  "";
    strokeOpacity: number =  0;
    strokeWeight: number =  0;
    fillColor: string = "";
    fillOpacity: number =  0; 

  clickedMarker(clickedMarker: any, index: number) {
    if(index !== 0){
        this.toggleComponets(2);
        let marker = this.mapMarkerModel(index);
        marker.streetViewImage = this.getStreetViewImagery(clickedMarker.lat, clickedMarker.lng);
        marker.latLng = {latitude: clickedMarker.lat, longitude: clickedMarker.lng};
        this.selectedMarker = marker;

        this.polygonColor = this.colors[index];
        this.drawPolygon(clickedMarker);
    }
    console.log(`clicked the marker: ${index} - ${this.selectedMarker.name}`)
  }

  drawPolygon(marker: any)
  {
     this.paths = [];
     this.unSortedArray = [];
     this.strokeColor = this.polygonColor;
     this.strokeOpacity = 0.8;
     this.strokeWeight = 0.1;
     this.fillColor = this.polygonColor;
     this.fillOpacity = 0.22;  

     let initLoc = {"latitude":marker.lat,"longitude":marker.lng};

     for (let _i= 1; _i < 10; _i++) 
     {
          this.unSortedArray.push(this.markerService.pointAtDistance(initLoc, 200, _i.toString()));
     }

     this.paths = this.unSortedArray.sort((n1,n2)=>
     { 
            if (n1.lat < n2.lat) {
                return 1;
            }
            if (n1.lat > n2.lat) {
                return -1;
            }
            return 0;    
     });
  }
  
  mapClicked($event: any) {
      this.paths = [];
      this.unSortedArray = [];
      this.map($event.coords.lat, $event.coords.lng);
      this.zoom = 18;    
      this.toggleComponets(1);            
  }
  
  markerDragEnd(m: any, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  
  ngOnInit()
  {
    this.apiLoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(document.getElementById('address'), {});
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                this.zone.run(() => {
                    let place = autocomplete.getPlace();
                    if (place.geometry.location) {
                        this.setPlaceDetails(place);                                               
                    }
                });
            });
        });
  }

  setPlaceDetails(place:any)
  {
      this.lat = place.geometry.location.lat();
      this.lng = place.geometry.location.lng();      
      this.map(this.lat, this.lng);
      this.zoom = 18;      
      this.toggleComponets(1);      
  }

  map(lati: number, long: number){
      this.markers = [];
      this.returnedMarkers = [];
      let initLoc = {"latitude":lati,"longitude":long};
      this.markers.push({lat:lati, lng:long,label:'',icon:ICONS_API + this.icons[0], draggable:false})  

      for (let _i= 1; _i < 5; _i++) {
          this.markers.push(this.markerService.pointAtDistance(initLoc, 100, _i.toString()));
          this.markers[_i].icon = _i <= 3 ? ICONS_API + this.icons[_i] : ICONS_API + this.icons[2];
          this.markers[_i].name = _i <= 3 ? this.labels[_i] : this.labels[2];
          this.markers[_i].latLng = {latitude: lati, longitude: long};
          this.returnedMarkers.push(this.markers[_i]);
      }
       
      this.lat = lati;
      this.lng = long;
  }

  toggleComponets(active: number){
      this.activePane = active;
      if(active == 0){
          this.paths = [];
         this.unSortedArray = [];
      }
  }

  mapMarkerModel(index: number){
    let marker = new MarkerModel();
    marker.name = index <= 3 ? this.labels[index] : this.labels[2];
    marker.icon = index <= 3 ? ICONS_API + this.icons[index] : ICONS_API + this.icons[2];
    return marker;
  }

  getStreetViewImagery(lat:number, lng:number){
      let location = lat + "," + lng;
      return STREETVIEW_API + location;
  }

  getDirections(latLng: any){
       this.destination = latLng;
       this.zoom = 18; 
       this.showDirections = true;
  }

   constructor (private apiLoader: MapsAPILoader, private zone: NgZone, private markerService: RandomMarkerGeneratorService) {  } 
}

export interface IMapper{
    map(lat: number, lng: number): void;
} 
