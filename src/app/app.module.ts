import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MouseEvent, AgmCoreModule } from 'angular2-google-maps/core';
import { AddressSearchComponent } from './address-search/address-search.component';
import { AddressSearchResultComponent } from './address-search-results/address-search-results.component';
import { MarkerModel } from './address-search-results/marker-details-model';
import { RandomMarkerGeneratorService } from './random-marker-generator.service';
import { MarkerDetailsComponent } from './marker-details/marker-details.component';
import { SebmGoogleDirectionsDirective } from './sebm-google-directions.directive';

@NgModule({
  declarations: [
    AppComponent,
    AddressSearchComponent,
    AddressSearchResultComponent,
    MarkerDetailsComponent,
    SebmGoogleDirectionsDirective
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAEXrOEWXaG4IAi0Dp0O6-8Muoc4ljUcLs',
      libraries: ['places'],
    }),
    HttpModule
  ],
  providers: [AppComponent, MarkerModel, RandomMarkerGeneratorService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA] 
})
export class AppModule { }
