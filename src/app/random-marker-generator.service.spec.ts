/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RandomMarkerGeneratorService } from './random-marker-generator.service';

describe('RandomMarkerGeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RandomMarkerGeneratorService]
    });
  });

  it('should ...', inject([RandomMarkerGeneratorService], (service: RandomMarkerGeneratorService) => {
    expect(service).toBeTruthy();
  }));
});
