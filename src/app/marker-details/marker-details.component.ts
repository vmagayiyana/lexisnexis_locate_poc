import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AppComponent } from '../app.component';

import { MarkerModel } from '../address-search-results/marker-details-model';

@Component({
  selector: 'app-marker-details',
  templateUrl: './marker-details.component.html',
  styleUrls: ['../app.component.css']
})
export class MarkerDetailsComponent {
  @Input()
  selectedMarker: MarkerModel;
  streetViewImage: any;

  @Input()
  destination: any;

  @Output()
  directions = new EventEmitter<any>();

  @Output()
  activePane = new EventEmitter<number>();

  drawDirections(){
      this.destination = this.selectedMarker.latLng;
      this.directions.emit(this.destination);
  }
  
  onCloseClick(){
      this.activePane.emit(0);
  }
}
