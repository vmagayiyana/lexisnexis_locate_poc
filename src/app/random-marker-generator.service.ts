import { Injectable } from '@angular/core';

const EARTH_RADIUS = 6371000;
const DEG_TO_RAD = Math.PI / 180.0;
const THREE_PI = Math.PI*3;
const TWO_PI = Math.PI*2;

const PLACES = {
	SWEDEN: {
		"latitude":-29.812,
		"longitude":30.996
	}
}

let initLoc = PLACES.SWEDEN;
let locations = [];
let MY_MAPTYPE_ID = 'custom_style';

@Injectable()
export class RandomMarkerGeneratorService {
    result: any = { };

    isFloat(num: any) {
      return !isNaN(parseFloat(num)) && isFinite(num);
    }

    recursiveConvert(input: any, callback: any){
      if (input instanceof Array) {
        return input.map((el) => this.recursiveConvert(el, callback));
      }
      if  (input instanceof Object) {
        input = JSON.parse(JSON.stringify(input));
        for (let key in input) {
          if( input.hasOwnProperty(key) ) {
            input[key] = this.recursiveConvert(input[key], callback);
          } 
        }
        return input;
      }
      if (this.isFloat(input)) { return callback(input); }
    }

    toRadians(input: any){
      return this.recursiveConvert(input, (val) => val * DEG_TO_RAD);
    }

    toDegrees(input: any){
      return this.recursiveConvert(input, (val) => val / DEG_TO_RAD);
    }

    pointAtDistance(inputCoords: any, distance: number, labelN: string) {
      const coords = this.toRadians(inputCoords);
      const sinLat = 	Math.sin(coords.latitude);
      const cosLat = 	Math.cos(coords.latitude);

      const bearing = Math.random() * TWO_PI;
      const theta = distance/EARTH_RADIUS;
      const sinBearing = Math.sin(bearing);
      const cosBearing = 	Math.cos(bearing);
      const sinTheta = Math.sin(theta);
      const cosTheta = 	Math.cos(theta);

      this.result = {
          lat : Math.asin(sinLat*cosTheta+cosLat*sinTheta*cosBearing)
      };
      this.result.lng = coords.longitude + Math.atan2( sinBearing*sinTheta*cosLat, cosTheta-sinLat*Math.sin(this.result.lat));
      this.result.lng = ((this.result.lng+THREE_PI)%TWO_PI)-Math.PI;
      return this.toDegrees(this.result);
    }

    pointInCircle(coord: any, distance: number) {
      const rnd =  Math.random();
      const randomDist = Math.pow(rnd,0.5) * distance;
      return this.pointAtDistance(coord, randomDist, rnd.toString());
    }
}

