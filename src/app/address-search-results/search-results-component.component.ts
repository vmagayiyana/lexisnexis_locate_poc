import { Component, Input } from '@angular/core';
import { AppComponent } from '../app.component';

import { MarkerModel } from './marker-details-model';

@Component({
  selector: 'app-address-search-results',
  templateUrl: './address-search-results.component.html',
  styleUrls: ['../app.component.css']
})

export class AddressSearchResultComponent{
  @Input()
  marker: MarkerModel;
}
