import { Component, EventEmitter ,Input, Output } from '@angular/core';
import { AppComponent } from '../app.component';

import { MarkerModel } from './marker-details-model';

@Component({
  selector: 'app-address-search-results',
  templateUrl: './address-search-results.component.html',
  styleUrls: ['../app.component.css']
})

export class AddressSearchResultComponent{
  @Input()
  markers: MarkerModel[];
  itemId: number;

  @Output()  
  clickedMarker = new EventEmitter<any>();

  @Output()
  activePane = new EventEmitter<number>();

  onItemClick(marker: any, index: number){
      this.itemId = index + 1;
      this.clickedMarker.emit({ event:marker, index: this.itemId });
  }

  onCloseClick(){
      this.activePane.emit(0);
  }
}
