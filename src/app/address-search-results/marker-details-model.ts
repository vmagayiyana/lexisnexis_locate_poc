import { Injectable } from '@angular/core';

@Injectable()
export class MarkerModel{
      id:number;
      name:string;
      icon:string;
      streetViewImage: any;
      latLng: any;
}