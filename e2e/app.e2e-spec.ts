import { LexisNexisPOCPage } from './app.po';

describe('lexis-nexis-poc App', function() {
  let page: LexisNexisPOCPage;

  beforeEach(() => {
    page = new LexisNexisPOCPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
